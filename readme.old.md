# Alternative GWT project setup

## Mission
Create alternative GWT project template ideal for multiple small applications, multiple EntryPoints, 
a playground project setup, where one click on EntryPoint class will launch browser will run that EntryPoint
and only that EntryPoint.

## Goals
 - Development friendly setup
 - Test, playground and technical spike style project template.
 - Perfect for ultra short modify-execute development cycles.
   Change is reflected immediately and browser opens with the right content. 
 - Focus only on Client Side code (Java to JS) - create environment perfect for simple testing of
   things like positioning, events, DOM manipulation, visual stuff,..
 - Maven only based setup. Rely only on Maven provided libraries. 
   No GWT and Web project plugin dependencies
 - Rely only on Code Server on-the-fly compilation from Java to JS. It is light fast this way!
 - Avoid slow full Java to JS compilation, either by Maven or Eclipse GWT plugin, in build time.
 - Avoid need of Eclipse launchers (currently necessary to add client source directories to classpath) 
 - start GWT app as easily as possible and as quickly as possible.
 - support large number of GWT apps easily in one project.
   Support one application == one class == one click to run. 
   Ideal for testing or demonstrating specific topics in isolation.
   Vanilla GWT is not very helpful in that.
 - Have more control on app start process. Be able to influence any part of the process, 
   be able to debug Code Server or Http Server. Expose what is under the hood of 
   individual components.
 - This setup is not intended for production code. Default setup created by Maven GWT plugin
   or Eclipse GWT is perfect for production indented project, stick with it.
 - No need to touch GWT module descriptor.  

## How to run it
 - Clone project
 - Run usual mvn clean install  
   Eclipse with m2e plugin provides convenient menu for it.  
   Ideally, you would need to run Maven just once, on the beginning, to set up everything. 
 - Write your entry EntryPoint class, perhaps testing some aspect of GWT, a class, a widget, layout, positioning trick..
   Class must be placed in the client code (currently my.code.client)   
   Check Sample1.java.
 - Write runner class, a class with main method, and one line of code, see Sample1Run.java. 
   More or less a copy and paste.
 - Run HttpServer (RunHttpServe.java)  
   If you forget, you will be kindly reminded, detection mechanism is in place.
   Keep this server running between individual your EntryPoint application runs.  
 - Run GWT CodeRerver (RunCodeServer.java)  
   If you forget, you will be kindly reminded, detection mechanism is in place.
   Keep this server running between individual your EntryPoint application runs.
 - Run your application, your EntryPoint, ideally with the prepared runner.
   Runner conveniently opens browser for you on the right URL, like this one
   http://localhost:8080/my.code.client.Sample3.do?gwt.codesvr=localhost:9998
 - Your Entry Point class is immediately executed and you are presented with results.
 - Edit your code, re-run your launcher, it should immediately reflect your changes.  
   Recompilation is fully transparent, courtesy of running Code Server, it is incremental, and is FAST.     
 - Edit, rer-un,  again, and again, ..
 - And new EntryPoint. No need to touch GWT module descriptor. Just add java file in the right package,
   extend EntryPoint and that's it. Create runner class, not essential but convenient. Alternative    
     
## How to run it - alternative
 - Creating a runner class for your Entry Point class is not essential but convenient.
   You can run you app by entering URL directly, the format is:
   http://localhost:8080/[qualified entry class point class name].do?gwt.codesvr=localhost:9998

------------------------------------------------------------------------------------------------

## Current situation (7.6.2013):
 - It somehow works. But a lot of compromises and is quite fragile.
 - Location of module descriptor (Sample.gwt.xml) seem to be correct. To be correct in relation to module name.
   If the "module name" is 'my.code.Sample' then module descriptor must be found as 'classpath:/my/code/Sample.gwt.xml'.
   (Node: I made experience that sometimes CS reacted on 
 - Code Server (DevMode) is started and seems to be working 
 - Code was added to Sample.java - centred box with lorem ipsum text - to demonstrate 
   that my client code was proceed.
 - Fire browser directly as exe, use firefox.exe, 
 - Lots of hardcoded paths everywhere - find how to share this information with Maven or rely only on classpath.
   Maven can fill some property file, then read by 
 - Sample can be run as Java program, but SRC directories have to be added 
   to classpath (launcher config).
 - Use open file:// in the browser, no http server needed - I'm reviewing this strategy, there 
   are several drawbacks - like cannot use Desktop.browse() because file URL are strip of
   query string, essential to establish communication with Code Server.
 - Browser (browser tab) with tested gwt app has closed before launching the same app again.
   Failing to do so results of nothing showing in the new tab/widow,
   user ends up staring on a blank page. Reason is unknown - client code not started? not compiled?
   some cached data still attached to still open old browser window/tab?
   
## Updates 8.6.2013
 - gwt-maven-plugin:2.5.1:compile complaints about:
   [WARNING] Don't declare gwt-dev as a project dependency. This may introduce complex dependency conflicts
   But I need gwt-dev - the essential class com.google.gwt.dev.DevMode, the Code Server, is there!
 - Eclipse issue after every mvn clean install:
   Project 'z025-gwt-maven-plugin' is missing required source folder: 'target/generated-sources/gwt'
   So I removed line '<classpathentry kind="src" path="target/generated-sources/gwt"/>' from .classpath 
   
## TODO (old):
 - CRITICAL: GWT's Code Server requires client source directories to be on the classpath (as first)
   requires creation of launcher :( Investigate if classpath can be modified in the program itself,
   before Code Server is launched.  
   DONE - sources added as resources so they are copied to output class folder (not the best but simplest solution)
 - Code Server seems to start too long (where it stores compiled classes? work directory? ensure it 
   reuses already compiled stuff)  
   SOLVED - make Code Server running as separate VM and keep it running between app invocations.  
 - research on what is "working directory" in GWT (where CS caches on the fly code?)
 - where and when Code Server compiles Java code to JS and where it stores results (temp directory) 
 - Use open file:// in the browser, no http server needed - I'm reviewing this strategy, there 
   are several drawbacks - like cannot use Desktop.browse() because file URL are strip of
   query string, essential to establish communication with Code Server.  
   SOLVED - make Http Server running as separate VM and keep it running between app invocations.
   Desktop.browse() can be now used as it deals with real (HTTP) URL. No more query string stripping experienced.
 - Share some set up data (web app directory) with Maven. 
   Code Server requires "war path" to be specified. It serves as base where to find application html.
   war path example: "C:\\Users\\espinosa\\workspace\\z025-gwt-maven-plugin\\target\\z025-gwt-maven-plugin-0.0.1-SNAPSHOT"
   Then Sample.html is expected to be in this directory.
   CS seems to expect fully absolute path (?).
   Note: this path is not in the classpath, at least not currently, what is on the classpath is:
   ../target/z025-gwt-maven-plugin-0.0.1-SNAPSHOT/WEB-INF/classes
   where all classes and resources are compiled.  
   SOLVED - Maven generates application properties file where the only directory not on the classpath
   the web app directory, is written there with every build (using Maven Resource Filters)  
 - prevent creation of project WAR file.  
   UNDECIDED - it does not really matter now much, Maven build is run on rare occasions only, and was now 
   does not contain any library so it is much smaller.
 - prevent copying jar library gwt-servlet to ../target/z025-gwt-maven-plugin-0.0.1-SNAPSHOT/WEB-INF/lib   
   SOLVED - ..and forgotten how. Probably by setting dependency scope as "provided".
 - create new git branch - 'alternative-setup'   
   DONE
 
## Updates 10.6.2013
Exhausting day today! Major milestone, rough edges everywhere, but all seem to be working as intended. 

 - One template html application page serves all requests.
 - Target entry point class is inserted to page as JavaScript global variable.
 - Use GWT native code to read JavaScript global variable.
 - Use Swizzle Stream library to replace token in html template.
 - Use GWT deferred binding and class Generator to mimic introspection, otherwise missing in GWT.
 - Provide 2 example mini applications - Sample1 and Sample2 and their launchers - Sample1Run and Sample2Run.
 - No hardcoded paths. Use Maven resource filters to generate application properties file with web app 
   root directory. Other paths are naturally deduced.
 
## Updates 11.6.2013
 - Change method how to detect HttpServer and CodeServer running. Use JXM 
   Introduce ActiveJavaVirtualMachineExplorer class.
 - Tools.jar are now required, add as a Maven dependency. This library is tricky, it is part of JDK and
   not in the Maven repo so SystemPath option has to be used. May not work correctly with some
   JDKs or Operating Systems.
 - Minor code clean up in RunnableEntryPoint class.
 - All seem to be running as supposed! No more stack traces from Code Server.
 
## TODOS and Left Overs
 - Consider making the whole web app a project resource. 
   Benefits: any change to CSS would be immediately reflected in application; no need to call Maven.
   New servlet would be needed - ExposeResourceDirectoryServlet (based on ExposeDirectoryServlet)
 - Check Maven resource definition, perhaps too restrictive excludes/includes.
 - gwt-dev should not be in the classpath. It contains potentially conflicting classes, like Apache IOUtils.
   The whole library is mess! But where else Ican get Code Server classes! (DevMode, ..)
 - Adding tools.jar poses potential risks; relying on SystemPath is tricky. There are also known
   issues with OSX systems having non-standard location, and openJDK allegedly have tools.jar already in the classpath. 
 - ActiveJavaVirtualMachineExplorer is not much tested
 - Much of the set up infrastructure is not tested. It is just on "works for me" basis.
 - test with different servlet context, currently it is /, but should be configurable (not essential)
 - Code Server prints annoing useless (?) exception when browser is closed
   com.google.gwt.dev.shell.BrowserChannel$RemoteDeathError: Remote connection lost
 
   