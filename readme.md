# Alternative GWT project setup

## Mission
Create alternative GWT project template ideal for multiple small applications, multiple EntryPoints, 
a playground project setup, where one click on EntryPoint class will launch browser will run that EntryPoint
and only that EntryPoint.

## Goals
 - Developers friendly setup
 - Test, playground and technical spike style project template.
 - Perfect for ultra short modify-execute development cycles.
   Change is reflected immediately and browser opens with the right content. 
 - Focus only on Client Side code (Java to JS) - create environment perfect for simple testing of
   things like positioning, events, DOM manipulation, visual stuff,..
 - Maven only based setup. Rely only on Maven provided libraries. 
   No GWT and Web project plugin dependencies
 - Rely only on Code Server on-the-fly compilation from Java to JS. It is light fast this way!
 - Avoid slow full Java to JS compilation, either by Maven or Eclipse GWT plugin, in build time.
 - Avoid need of Eclipse launchers (currently necessary to add client source directories to classpath) 
 - start GWT app as easily as possible and as quickly as possible.
 - support large number of GWT apps easily in one project.
   Support one application == one class == one click to run. 
   Ideal for testing or demonstrating specific topics in isolation.
   Vanilla GWT is not very helpful in that.
 - Have more control on app start process. Be able to influence any part of the process, 
   be able to debug Code Server or Http Server. Expose what is under the hood of 
   individual components.
 - This setup is not intended for production code. Default setup created by Maven GWT plugin
   or Eclipse GWT is perfect for production indented project, stick with it.
 - No need to touch GWT module descriptor.  

## How to run it
 - Clone project
 - Run usual mvn clean install  
   Eclipse with m2e plugin provides convenient menu for it.  
   Ideally, you would need to run Maven just once, on the beginning, to set up everything. 
 - Write your entry EntryPoint class, perhaps testing some aspect of GWT, a class, a widget, layout, positioning trick..
   Class must be placed in the client code (currently my.code.client)   
   Check Sample1.java.
 - Write runner class, a class with main method, and one line of code, see Sample1Run.java. 
   More or less a copy and paste.
 - Run HttpServer (RunHttpServe.java)  
   If you forget, you will be kindly reminded, detection mechanism is in place.
   Keep this server running between individual your EntryPoint application runs.  
 - Run GWT CodeRerver (RunCodeServer.java)  
   If you forget, you will be kindly reminded, detection mechanism is in place.
   Keep this server running between individual your EntryPoint application runs.
 - Run your application, your EntryPoint, ideally with the prepared runner.
   Runner conveniently opens browser for you on the right URL, like this one
   http://localhost:8080/my.code.client.Sample3.do?gwt.codesvr=localhost:9998
 - Your Entry Point class is immediately executed and you are presented with results.
 - Edit your code, re-run your launcher, it should immediately reflect your changes.  
   Recompilation is fully transparent, courtesy of running Code Server, it is incremental, and is FAST.     
 - Edit, rerun,  again, and again, ..
 - And new EntryPoint. No need to touch GWT module descriptor. Just add java file in the right package,
   extend EntryPoint and that's it. Create runner class, not essential but convenient. Alternative    
     
## How to run it - alternative
 - Creating a runner class for your Entry Point class is not essential but convenient.
   You can run you app by entering URL directly, the format is:
   http://localhost:8080/[qualified entry class point class name].do?gwt.codesvr=localhost:9998

## TODOs and leftovers
 - Consider making the whole web app a project resource. That is everything will end up on classpath.
   Benefits: any change to CSS would be immediately reflected in application; no need to call Maven.
   New servlet would be needed - ExposeResourceDirectoryServlet (based on ExposeDirectoryServlet)
 - Check Maven resource definition, perhaps too restrictive excludes/includes.
 - gwt-dev should not be in the classpath. It contains potentially conflicting classes, like Apache IOUtils.
   The whole library is mess! But where else Ican get Code Server classes! (DevMode, ..)
 - Adding tools.jar poses potential risks; relying on SystemPath is tricky. There are also known
   issues with OSX systems having non-standard location, and openJDK allegedly have tools.jar already in the classpath. 
 - ActiveJavaVirtualMachineExplorer is not much tested
 - Much of the set up infrastructure is not tested. It is just on "works for me" basis.
 - test with different servlet context, currently it is /, but should be configurable (not essential)
 - Code Server prints annoing useless (?) exception when browser is closed
   com.google.gwt.dev.shell.BrowserChannel$RemoteDeathError: Remote connection lost
 
   