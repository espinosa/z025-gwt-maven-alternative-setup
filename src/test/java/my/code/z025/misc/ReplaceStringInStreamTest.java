package my.code.z025.misc;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

import org.apache.commons.io.IOUtils;
import org.codehaus.swizzle.stream.FixedTokenListReplacementInputStream;
import org.codehaus.swizzle.stream.StringTokenHandler;
import org.junit.Test;

public class ReplaceStringInStreamTest {

	/**
	 * Test swizzle-stream (org.codehaus.swizzle:swizzle-stream) library text replacing in stream together
	 * with Apache common-io library, notably {@link IOUtils#contentEquals(InputStream, InputStream)}.
	 * See sizzle's {@link FixedTokenListReplacementInputStream} and {@link StringTokenHandler}.
	 * <p>
	 * This test does not work in 1.6.1 version of swizzle-stream, it works again with newest 1.6.2
	 * version.
	 * @since 10.6.2013
	 */
	@Test
	public void test() throws IOException {
		String originalText = "" +
			"    <script type=\"text/javascript\" language=\"javascript\">\n" +
			"    var targetEntryPointClass='@@ENTRY_POINT_CLASS_NAME@@'\n" +
			"    </script>";
		String expectedText = "" +
			"    <script type=\"text/javascript\" language=\"javascript\">\n" +
			"    var targetEntryPointClass='my.code.client.Sample2'\n" +
			"    </script>";

		InputStream originalInputStream = IOUtils.toInputStream(originalText);
		;
		InputStream modifiedTemplateStream = new FixedTokenListReplacementInputStream(originalInputStream,
			Arrays.asList("@@ENTRY_POINT_CLASS_NAME@@"),
			new StringTokenHandler() {
				public String handleToken(String token) {
					// simplest possible implementation of StringTokenHandler - it just 
					// replaces one string with another. No need to even check the token.
					return "my.code.client.Sample2";
				}
			});
		OutputStream out = new ByteArrayOutputStream();
		IOUtils.copy(modifiedTemplateStream, out);
		assertEquals(expectedText, out.toString());
	}
}
