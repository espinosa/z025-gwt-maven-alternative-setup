package my.code.z025.misc;

import java.util.List;

import my.code.z025.util.ActiveJavaVirtualMachineExplorer;
import my.code.z025.util.ActiveJavaVirtualMachineExplorer.ActiveVm;

import org.junit.Test;

public class ListLocalRunningJVMsTest {
	
	@Test
	public void test1() {
		List<ActiveVm> activeVms = ActiveJavaVirtualMachineExplorer.getActiveLocalVms();
		for (ActiveVm activeVm : activeVms) {
			System.out.println(activeVm);
		}
	}
}
