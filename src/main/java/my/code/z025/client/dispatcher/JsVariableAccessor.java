package my.code.z025.client.dispatcher;

/**
 * Retrieve selected global JavaScript variable values from browser.
 * <p>
 * See http://stackoverflow.com/questions/14563817/accessing-javascript-variable-in-gwt
 * See https://developers.google.com/web-toolkit/doc/latest/DevGuideCodingBasicsJSNI#writing
 */
public class JsVariableAccessor {
	
	/**
	 * Retrieve target EntryPoint class name from a global JavaScript variable 'targetEntryPointClass'.
	 * @return target EntryPoint class name
	 */
	public static native String getTargetEntryPointClass()
	/*-{
	return $wnd.targetEntryPointClass
	}-*/;
}
