package my.code.z025.client;

import my.code.z025.client.dispatcher.ClassFromStringFactory;
import my.code.z025.client.dispatcher.JsVariableAccessor;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.shared.GWT;

/**
 * Dispatching Entry Point. Core part of this alternative maven setup. When invoked it delegates
 * requested Entry Point, the real application entry point. Global JavaScript variable
 * 'targetEntryPointClass' holds name of EntryPoint class to be dispatched
 */
public class Dispatcher implements EntryPoint {

	/**
	 * When invoked delegate requested Entry Point, the real application entry point. Global
	 * JavaScript variable 'targetEntryPointClass' holds name of EntryPoint class to be dispatched
	 */
	public void onModuleLoad() {
		String targetEntryPointClass = JsVariableAccessor.getTargetEntryPointClass();
		ClassFromStringFactory classFromStringFactory = GWT.create(ClassFromStringFactory.class);
		Object targetEntryPointInstance = classFromStringFactory.instantiate(targetEntryPointClass);
		if (targetEntryPointInstance == null) {
			throw new IllegalArgumentException(
				"Class " + targetEntryPointClass + " is not supported by " + ClassFromStringFactory.class);
		}
		if (targetEntryPointInstance instanceof EntryPoint) {
			((EntryPoint) targetEntryPointInstance).onModuleLoad();
		} else {
			throw new RuntimeException("Class " + targetEntryPointInstance + " is not EntryPoint class");
		}
	}
}
