package my.code.z025.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Sample runnable one class only application. This GWT Maven setup support galore of such in one
 * project.
 */
public class Sample2 implements EntryPoint {

	String panelContent = "BBB Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum";

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		final AbsolutePanel panel = new AbsolutePanel();
		panel.setPixelSize(400, 400);
		panel.setStyleName("main-panel");
		panel.add(new HTML(panelContent));
		RootPanel.get().add(panel);
		centerComponent(panel, (AbsolutePanel)panel.getParent());

		Window.addResizeHandler(new ResizeHandler() {
			@Override
			public void onResize(ResizeEvent event) {
				centerComponent(panel, (AbsolutePanel)panel.getParent());
			}
		});
	}
	
	public void centerComponent(AbsolutePanel panel, AbsolutePanel parent) {
		int screenWidth = Window.getClientWidth();
		int screenHeight = Window.getClientHeight();
		int width = panel.getOffsetWidth();
		int height = panel.getOffsetHeight();
		int left = (screenWidth - width) / 2;
		int top = (screenHeight - height) / 2;
		parent.setWidgetPosition(panel, left, top);
	}
}
