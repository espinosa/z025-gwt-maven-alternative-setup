package my.code.z025.launch;

import my.code.z025.client.Sample1;

public class Sample1Run {

	public static void main(String[] args) {
		my.code.z025.util.RunnableEntryPoint.run(Sample1.class);
	}

	// Hopefully, this launcher will not be soon necessary. Compiler annotation was recently
	// submitted to GWT to prevent compilation of a method: 
	// See: http://stackoverflow.com/questions/13184881/annotation-for-gwt-compiler-to-ignore-method/16989956#16989956
	// This would enable to put main() method directly to EntryPoint class
}
