package my.code.z025.launch;

import java.io.File;
import java.net.URL;
import java.util.Arrays;

import my.code.z025.util.Configuration;
import my.code.z025.util.SetupException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gwt.dev.DevMode;

/**
 * Start headless GWT Code Server. Run this before any GWT application and keep it running.
 * Code Server should not share same JVM as the GWT application.
 * This two prerequisites makes starting GWT application much quicker. 
 */
public class RunCodeServer {
	private Logger logger = LoggerFactory.getLogger(RunCodeServer.class);

	public static void main(String[] args) {
		new RunCodeServer().doRun();
	}

	protected void doRun() {
		checkThatJavaSourcesAndModuleDescriptorAreOnTheClasspath();
		launchCodeServer();
	}

	/**
	 * GWT Code Server requires to have all client sources, the java source files, on the classpath.
	 * Same for module descriptor file. Check that on two prominent files - module descriptor xml
	 * and dispatcher entry point class.
	 */
	protected void checkThatJavaSourcesAndModuleDescriptorAreOnTheClasspath() {
		URL testModFile1 = ClassLoader.getSystemResource(Configuration.getRelativePathToDispatcherGwtFile());
		if (testModFile1 == null) {
			throw new SetupException("" +
				"Prerequise check failure. The GWT module descriptor file " +
				Configuration.getRelativePathToDispatcherGwtFile()
				+ " was not recognized on the classpath. It is required to be there by GWT CodeServer.");
		}
		URL testModFile2 = ClassLoader.getSystemResource(Configuration.getRelativePathToDispatcherJavaFile());
		if (testModFile2 == null) {
			throw new SetupException("" +
				"Prerequise check failure. A module source file " +
				Configuration.getRelativePathToDispatcherJavaFile()
				+ " was not recognized on the classpath. It is required to be there by GWT CodeServer.");
		}
	}

	/**
	 * Fire the code server.
	 */
	protected void launchCodeServer() {
		final DevMode hostedMode = new HeadlessGwtCodeServer(
			Configuration.MODULE_NAME,
			Configuration.getWebAppDirOnLocalFS(),
			Configuration.CODE_SERVER_PORT
			);
		new Thread(new Runnable() {
			@Override
			public void run() {
				hostedMode.run();
			}
		}).start();
		logger.info("Code Server started successfully");
	}

	/**
	 * Extend GWT Code Server (DevMode) to make equivalent of calling DevMode tool with this
	 * commandline arguments:
	 * 
	 * <pre>
	 * -noserver
	 * -war C:\Users\espinosa\workspace\z025-gwt-maven-plugin\target\z025-gwt-maven-plugin-0.0.1-SNAPSHOT  
	 * -logLevel INFO
	 * -codeServerPort 9998
	 * my.code.Sample
	 * <pre>
	 */
	public static class HeadlessGwtCodeServer extends DevMode {

		public HeadlessGwtCodeServer(
			String moduleName,
			String warPath,
			int codeServerPort)
		{
			super();

			this.options.setCodeServerPort(codeServerPort);
			this.options.setWarDir(new File(warPath));
			this.options.setModuleNames(Arrays.asList(moduleName));

			// set defaults
			this.options.setUseGuiLogger(false);
			this.options.setLogLevel(com.google.gwt.core.ext.TreeLogger.Type.INFO);
			//this.options.setLogLevel(com.google.gwt.core.ext.TreeLogger.Type.DEBUG);
			this.options.setNoServer(true); // forbid starting a http server instance (usually Jetty)

			setHeadless(true);
		}

	}
}
