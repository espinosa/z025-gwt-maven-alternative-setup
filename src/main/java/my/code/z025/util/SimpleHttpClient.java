package my.code.z025.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.io.IOUtils;

/**
 * Lightweight variant to proper http client, like Apache HttpClient. Also see
 * {@link HttpURLConnection}. Used to get result from ping request to HttpServer when checking
 * HttpServer liveness.
 * 
 * @author espinosa
 * @since 9.6.2013
 */
public class SimpleHttpClient {

	/**
	 * @param url
	 *            requestd page
	 * @return content of a requested HTTP resource like HTML page.
	 */
	public static String get(String url) {
		return new SimpleHttpClient().callGet(url);
	}

	protected String callGet(String urlStr) {
		URL url;
		URLConnection conn;
		InputStream is = null;
		StringWriter sw = new StringWriter();
		try {
			url = new URL(urlStr);
			conn = url.openConnection();
			is = conn.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			IOUtils.copy(rd, sw);
			return sw.toString();
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			IOUtils.closeQuietly(is);
		}
	}
}
