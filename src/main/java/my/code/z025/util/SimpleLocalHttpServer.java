package my.code.z025.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.codehaus.swizzle.stream.FixedTokenListReplacementInputStream;
import org.codehaus.swizzle.stream.StringTokenHandler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import com.google.gwt.core.client.EntryPoint;

/**
 * Simple local HTTP server. No WAR or web.xml required, uses builder pattern interface instead.
 * Also provides 3 Servlet implementations to be used with this server.
 * 
 * Example of use:
 * 
 * <pre>
 * {@code
 * new SimpleLocalHttpServer(8080)
 * 	.withContext("/")
 * 	.addServlet(
 * 		new FooServlet1(), "/foo1") // absolute defined servlet mapping
 * 	.addServlet(
 * 		new FooServlet2(), "/*.do") // suffix based servlet mapping
 * 	.addServet(
 * 		new FooServlet3(), "/") // &quot;else&quot; mapping, everything else what was not matched elsewhere goes here
 * 	.start();
 * }
 * </pre>
 * 
 * @author espinosa
 * @since 8.6.2013
 */
public class SimpleLocalHttpServer {

	private final Server server;

	private final int port;

	public SimpleLocalHttpServer(int port) {
		this.port = port;
		server = new Server(port);
	}

	/**
	 * Start the server when it is fully configured
	 */
	public void start() {
		try {
			server.start();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Set context path. When context path is specified, servlets can be added to it.
	 * 
	 * @param contextPath
	 *            example: "/"
	 * @return {@link SimpleLocalHttpServerContext} for servlets to be added to it.
	 */
	public SimpleLocalHttpServerContext withContext(String contextPath) {
		ServletContextHandler context = new ServletContextHandler();
		context.setContextPath(contextPath);
		server.setHandler(context);
		return new SimpleLocalHttpServerContext(this, context);
	}

	/**
	 * When context path is specified, only then servlets can be added to it.
	 */
	public static class SimpleLocalHttpServerContext {

		private final SimpleLocalHttpServer server;
		private final ServletContextHandler context;

		public SimpleLocalHttpServerContext(SimpleLocalHttpServer server, ServletContextHandler context) {
			this.server = server;
			this.context = context;
		}

		/**
		 * Add servlet to http server context
		 * 
		 * @param servlet
		 *            example: new ExposeDirectoryServlet(...)
		 * @param pathPattern
		 *            example: "/*"
		 * @return this instance for easy chaining
		 */
		public SimpleLocalHttpServerContext addServlet(HttpServlet servlet, String pathPattern) {
			context.addServlet(new ServletHolder(servlet), pathPattern);
			return this;
		}

		/**
		 * Shortcut to context hosting http server start method
		 */
		public void start() {
			server.start();
		}
	}

	/**
	 * Stop http server. Not explicitly used, server process is safe and moreconvenient to siply
	 * kill from IDE.
	 */
	public void stop() {
		try {
			server.stop();
		} catch (Exception e) {
			throw new SetupException(e);
		}
	}

	/**
	 * @return http server port, a configuration parameter
	 */
	public int getPort() {
		return port;
	}

	/**
	 * Expose local file system directory for http requests. Useful for simple access to static
	 * content like CSS files, static HTML, JavaScript files, GWT javaScript stuff, pictures.
	 * <p>
	 * Example: {@code GET http://localhost:8080/Sample/Sample.css} returns content of a file:
	 * {@code C:\Users\espinosa\workspace\z025-gwt-maven-plugin\target\z025-gwt-maven-plugin-0.0
	 * .1-SNAPSHOT/Sample/Sample.css}
	 */
	public static class ExposeDirectoryServlet extends HttpServlet {
		private static final long serialVersionUID = 1L;

		/**
		 * All requested files are relative to this path Example:
		 * C:\Users\espinosa\workspace\z025-gwt-maven-plugin\target\z025-gwt-maven-plugin-0.0
		 * .1-SNAPSHOT
		 */
		public final String rootDirectoryPath;

		/**
		 * Setting constructor
		 * 
		 * @param rootDirectoryPath
		 *            example:
		 *            C:\Users\espinosa\workspace\z025-gwt-maven-plugin\target\z025-gwt-maven
		 *            -plugin-0.0 .1-SNAPSHOT
		 */
		public ExposeDirectoryServlet(String rootDirectoryPath) {
			this.rootDirectoryPath = rootDirectoryPath;
		}

		/**
		 * Example:<br>
		 * {@code GET http://localhost:8080/Sample.html} returns data of Sample.html from local file
		 * system<br>
		 * {@code GET http://localhost:8080/Sample/Sample.nocache.js} returns data of
		 * Sample/Sample.nocache.js from local file system<br>
		 * File paths are relative to root path.
		 */
		public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			// TODO: what about context path?
			//String requestedFileRelativePath = req.getPathInfo();
			String requestedFileRelativePath = req.getServletPath(); // example: "/Sample.css"
			File parentDir = new File(rootDirectoryPath);
			File requestedFile = new File(parentDir, requestedFileRelativePath);
			if (!requestedFile.exists()) {
				System.err.println("Requested path not found; requestedFileRelativePath: " +
					requestedFileRelativePath +
					"; Absolute path: " + requestedFile.getAbsolutePath());
				resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
				return;
			}
			if (!requestedFile.isFile()) {
				System.err.println("Requested path not a file; requestedFileRelativePath: " +
					requestedFileRelativePath +
					"; Absolute path: " + requestedFile.getAbsolutePath());
				resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
				return;
			}
			if (!requestedFile.canRead()) {
				System.err.println("Requested file cannot be read; requestedFileRelativePath: " +
					requestedFileRelativePath +
					"; Absolute path: " + requestedFile.getAbsolutePath());
				resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
				return;
			}
			InputStream fileInputStream = new BufferedInputStream(new FileInputStream(requestedFile));
			resp.setStatus(HttpServletResponse.SC_OK);
			IOUtils.copy(fileInputStream, resp.getOutputStream());
		}
	};

	/**
	 * Extremely simple servlet, just to check if server is up and running. Return OK token on one
	 * specific URL.
	 */
	public static class PingServlet extends HttpServlet {
		private static final long serialVersionUID = 1L;

		public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			resp.getOutputStream().write("OK".getBytes());
		}
	}

	/**
	 * Loads general application HTML as template, sets target entry point class to it, entry point
	 * class name is based on requested name and present result as if the entry point was invoked.
	 * <p>
	 * Example:<br>
	 * {@code GET http://localhost:8080/Test123?param1=abc}<br>
	 * loads {@code Dispatcher.html} where targetEntryPointClass is set to
	 * {@code my.code.client.Test123} and dispatcher instantiates and runs my.code.client.Test123 as
	 * the real {@link EntryPoint}.
	 * 
	 * @since 10.6.2013
	 */
	public static class GwtDispatcherServlet extends HttpServlet {
		private static final long serialVersionUID = 1L;

		/**
		 * Example:
		 * C:\Users\espinosa\workspace\z025-gwt-maven-plugin\target\z025-gwt-maven-plugin-0.0
		 * .1-SNAPSHOT
		 */
		public final String webAppDirPath;

		public final String regexToExtractTargetClass;

		public final String templateHtmlFileName;

		public GwtDispatcherServlet(String webAppDirPath,
			String regexToExtractTargetClass, String templateHtmlFileName)
		{
			this.webAppDirPath = webAppDirPath;
			this.regexToExtractTargetClass = regexToExtractTargetClass;
			this.templateHtmlFileName = templateHtmlFileName;
		}

		/**
		 * Example {@code GET http://localhost:8080/my.code.client.Sample1.do} returns data of
		 * template application html, Dispatcher.html, with inserted target EntryPoint class name,
		 * let say "my.code.client.Sample1", so it can be picked up by Dispatcher Entry Point.
		 * Dispatcher will then instantiate and run the requested class, calls
		 * Sample1#onModuleLoad().
		 */
		public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			// get requested path from HTTP request, only path, no query string, etc, ..
			// TODO: what about context path?

			// for difference between getServletPath() and getPathInfo()
			// http://www.coderanch.com/t/553443/Servlets/java/request-getPathInfo-request-getServletPath
			String requestedPath = req.getServletPath();

			// extract entry point class name from it
			Pattern p = Pattern.compile(regexToExtractTargetClass);
			Matcher m = p.matcher(requestedPath);

			String entryClassName = null;
			if (m.find()) {
				entryClassName = m.group(1);
			} else {
				System.err.println("The requestedPath "
					+ requestedPath + " cannot be matched with entry point class name extractor");
				resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				return;
			}

			// extend extracted entryClassName to fully qualified class name
			// example: Test123 -> my.code.Test123
			//final String entryClassQualifiedName = packagePrefixForTargetEntryPointClass + "." + entryClassName;
			final String entryClassQualifiedName = entryClassName;

			InputStream fileInputStream = retrieveHtmlTemplate();

			// set entryClassQualifiedName to output stream, using template as base
			InputStream modifiedTemplateStream = new FixedTokenListReplacementInputStream(fileInputStream,
				Arrays.asList("@@ENTRY_POINT_CLASS_NAME@@"),
				new StringTokenHandler() {
					public String handleToken(String token) {
						// define global JavaScript variable (Window scope)
						return entryClassQualifiedName;
					}
				});

			resp.setStatus(HttpServletResponse.SC_OK);
			IOUtils.copy(modifiedTemplateStream, resp.getOutputStream());
		}

		/**
		 * @return application HTML file, the template, as stream. It will be modified on-the-fly
		 */
		private InputStream retrieveHtmlTemplate() {
			File parentDir = new File(webAppDirPath);
			File templateHtmlFile = new File(parentDir, templateHtmlFileName);
			if (!templateHtmlFile.exists()) {
				throw new SetupException("Template html file does not exist: " + templateHtmlFile.getAbsolutePath());
			}
			if (!templateHtmlFile.isFile()) {
				throw new SetupException("Template html file is not a file: " + templateHtmlFile.getAbsolutePath());
			}
			if (!templateHtmlFile.canRead()) {
				throw new SetupException("Template html file cannot be read: " + templateHtmlFile.getAbsolutePath());
			}
			try {
				return new BufferedInputStream(new FileInputStream(templateHtmlFile));
			} catch (FileNotFoundException e) {
				throw new SetupException(
					"IOException when opening template html file: " + templateHtmlFile.getAbsolutePath());
			}
		}
	};
}
