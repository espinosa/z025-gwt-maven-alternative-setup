package my.code.z025.util;

/**
 * Encapsulates exceptions for any Maven Alternative Setup issues, with
 * like required server is not running, or configuration mistakes, .. 
 */
public class SetupException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public SetupException(Throwable cause) {
		super(cause);
	}
	
	public SetupException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public SetupException(String message) {
		super(message);
	}
}
