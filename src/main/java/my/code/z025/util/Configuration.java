package my.code.z025.util;

import java.io.IOException;
import java.util.Properties;

import my.code.z025.client.Dispatcher;

/**
 * 
 * TODO: put more values the application properties file. 
 * 
 * @since 10.6.2013
 */
public class Configuration {

	public static final String MODULE_NAME = "my.code.z025.Dispatcher";
	
	public static final String MODULE_DESCRIPTOR_FILE_REL_PATH = "my/code/z025/Dispatcher.gwt.xml";

	public static final String HOST_NAME = "localhost";

	public static final int HTTP_SERVER_PORT = 8080;

	public static final String APP_CONTEXT = "/";

	public static final String HTML_TEMPLATE_FILE = "Dispatcher.html";

	public static final String PING_APP_PATH = "ping";

	public static final int CODE_SERVER_PORT = 9998;

	public static final String GWT_CODE_SRV_PARAM = "gwt.codesvr=" + HOST_NAME + ":" + CODE_SERVER_PORT;
	
	public static final String REGEX_TO_EXTRACT_CLASS_NAME_FROM_SERVLET_NAME = "/(.*).do";
	
	public static final String DISPATCHER_SERVLET_MAPPING_STRING = "*.do";

	/**
	 * Return application URL to be open in the browser for developer convenience.
	 * 
	 * @param appClassName
	 *            example "my.code.Dispatcher"
	 * @return example: http://localhost:8080/my.code.Dispatcher.do?gwt.codesvr=127.0.0.1:9998
	 */
	public static String getAppUrl(String appClassName) {
		return "http://" + HOST_NAME + ":" + HTTP_SERVER_PORT + APP_CONTEXT + appClassName + "?" + GWT_CODE_SRV_PARAM;
	}

	/**
	 * Return "ping" URL to check if the Http Server is running
	 * 
	 * @return example: http://localhost:8080/ping
	 */
	public static String getPingAppUrl() {
		return "http://" + HOST_NAME + ":" + HTTP_SERVER_PORT + "/" + PING_APP_PATH;
	}

	public static String getWebAppDirOnLocalFS() {
		return WEB_APP_PATH;
	}

	public static Properties properties = new Properties();

	static {
		try {
			properties.load(ClassLoader.getSystemResourceAsStream("application.properties"));
		} catch (IOException e) {
			throw new SetupException(e);
		}
	}

	public static final String WEB_APP_PATH = properties.getProperty("webappDirectory");

	/**
	 * @return something like my/code/z025/Dispatcher.gwt.xml
	 */
	public static String getRelativePathToDispatcherGwtFile() {
		return MODULE_DESCRIPTOR_FILE_REL_PATH;
	}

	/**
	 * @return something like my/code/z025/client/Dispatcher.java
	 */
	public static String getRelativePathToDispatcherJavaFile() {
		return Dispatcher.class.getName().replace('.', '/') + ".java";
	}
}
