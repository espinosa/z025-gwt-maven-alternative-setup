package my.code.z025.util;

import java.io.IOException;
import java.net.URISyntaxException;

import my.code.z025.launch.RunCodeServer;
import my.code.z025.launch.RunHttpServer;

import com.google.gwt.dev.util.BrowserLauncher;

/**
 * Convenient client application launcher. Check that Code Server and Http Servers are running and
 * opens a web browser on the correct URL, opens the application for user, straight to the point.
 * <p>
 * Example:
 * 
 * <pre>
 * public class Sample1Run {
 * 	public static void main(String[] args) {
 * 		RunnableEntryPoint.run(Sample1.class);
 * 	}
 * }
 * </pre>
 */
public class RunnableEntryPoint {

	public static void run(Class<?> entryPointClazz) {
		new RunnableEntryPoint().doRun(entryPointClazz.getName());
	}

	protected void doRun(String entryPointClassName) {
		checkThatCodeServerIsRunning();
		checkThatHttpServerIsRunning();
		launchBrowser(entryPointClassName);
	}

	protected void checkThatCodeServerIsRunning() {
		if (!ActiveJavaVirtualMachineExplorer.checkActiveJvmForMainClass(RunCodeServer.class)) {
			throw new SetupException("" +
				"GWT Code Server was not detected as running. " +
				"Before you run RunnableEntryPoint application ensure that you run " +
				RunCodeServer.class.getName() + " first. " +
				"It is recommended to run as separate process and keep it running.");
		}
	}

	protected void checkThatHttpServerIsRunning() {
		if (!ActiveJavaVirtualMachineExplorer.checkActiveJvmForMainClass(RunHttpServer.class)) {
			throw new SetupException("" +
				"Supporting HTTP server was not detected as running. " +
				"Before you run RunnableEntryPoint application ensure that you run " +
				RunHttpServer.class.getName() + " first. " +
				"It is recommended to run as separate process and keep it running.");
		}
	}

	/**
	 * Launch main GWT project HTML page in system web browser.
	 * <p>
	 * Opens browser on application URL like this one:<br>
	 * {@code http://localhost:8080/my.code.Test123.do?gwt.codesvr=127.0.0.1:9998}
	 */
	protected void launchBrowser(String entryPointClassName) {
		String appUrl = Configuration.getAppUrl(entryPointClassName + ".do");
//		try {
//			Desktop.getDesktop().browse(URI.create(sampleUrl));
//		} catch (IOException e) {
//			throw new RuntimeException(e);
//		}
		
		try {
			BrowserLauncher.browse(appUrl);
		} catch (IOException e) {
			throw new SetupException("Launching web browser failed");
		} catch (URISyntaxException e) {
			throw new SetupException("Launching web browser failed");
		}
	}
}
